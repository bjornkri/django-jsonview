from django.http import HttpResponse
from django.views.generic import DetailView
import simplejson as json


class JSONView(DetailView):
    method_name = 'json_dict'

    def render_to_response(self, context):
        try:
            json_dict = getattr(self.object, self.method_name)()
        except AttributeError, e:
            json_dict = {'error': e}
        return HttpResponse(json.dumps(json_dict),
            mimetype="application/json")
