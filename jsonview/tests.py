from django.test import TestCase
from jsonview.views import JSONView
from django.db import models


class DummyModel(models.Model):
    title = models.CharField(max_length=255)

    def json_method(self):
        return {}


class JSONTest(TestCase):
    def setUp(self):
        self.dm = DummyModel.objects.create(title='Test!')

    def test_method_missing(self):
        view = JSONView(object=DummyModel.objects.get(id=1))
        response = view.render_to_response('dummy')
        self.assertEqual(response.content,
            '{"error": "Expected method `json_dict()`"}')

    def test_method(self):
        view = JSONView(object=DummyModel.objects.get(id=1),
            method_name='json_method')
        response = view.render_to_response('dummy')
        self.assertEqual(response.content, '{}')
