from distutils.core import setup

setup(name='django-jsonview',
    version='1.0',
    description='A Django DetailView that returns JSON.',
    author='Bjorn Kristinsson',
    author_email='bjorn@bjornkri.com',
    url='https://bitbucket.org/bjornkri/django-jsonview/',
    packages=['jsonview'],
    )
