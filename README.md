# django-jsonview

A simple Django DetailView to return a JSON string from a model. The model has to implement a method, `json_dict` by default, that returns a dictionary that must be converted to JSON.

Just add this to your urlconf:

    from jsonview.views import JSONView
    
    urlpatterns = patterns('',
        ...
        url(r'^mymodel/json/(?P<pk>\d+)/$', JSONView.as_view(
            model=MyModel), name='mymodel_json')
        ...
    )

You can also specify the `method_name`, which is useful if you need different data for different purposes.

    url(r'^mymodel/json/(?P<pk>\d+)/$', JSONView.as_view(
        method_name='json_chart', model=MyModel), 
        name='mymodel_json_chart'),

This would call the method `json_chart()` on `MyModel`.
